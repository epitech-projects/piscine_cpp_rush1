/*
** new.c for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush1/ex_01
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Sat Jan 11 17:58:58 2014 Jean Gravier
** Last update Sun Jan 12 04:39:56 2014 Jean Gravier
*/

#include "new.h"
#include "raise.h"
#include "point.h"
#include <string.h>
#include <stdlib.h>

void*		new(Class* class)
{
  Object	*obj;

  if (class == NULL)
    raise("Invalid pointer.");

  if ((obj = malloc(class->__size__)) == NULL)
    raise("Out of memory");
  obj = memcpy(obj, class, class->__size__);
  if (class->__init__)
    class->__init__(obj);
  return (obj);
}

void	delete(Object* ptr)
{
  if (Point->__del__ != NULL && ptr != NULL)
    {
      Point->__del__(ptr);
      free(ptr);
      ptr = NULL;
    }
   return;
}
