/*
** new.c for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush1/ex_01
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Sat Jan 11 17:58:58 2014 Jean Gravier
** Last update Sun Jan 12 04:50:46 2014 Jean Gravier
*/

#include "new.h"
#include "raise.h"
#include "point.h"
#include "object.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

Object*		new(Class* class, ...)
{
  va_list	ap;
  Object	*obj;

  if (class == NULL)
    raise("Invalid pointer.");

  va_start(ap, class);
  if ((obj = malloc(class->__size__)) == NULL)
    raise("Out of memory");
  obj = memcpy(obj, class, class->__size__);
  if (class->__init__)
    class->__init__(obj, &ap);
  va_end(ap);
  return (obj);
}

Object*		va_new(Class* class, va_list* ap)
{
  Object	*obj;

  if (class == NULL || ap == NULL)
    raise("Invalid pointer.");

  if ((obj = malloc(class->__size__)) == NULL)
    raise("Out of memory");
  obj = memcpy(obj, class, class->__size__);
  if (class->__init__)
    class->__init__(obj, ap);
  return (obj);
}

void	delete(Object* ptr)
{
  if (Point->__del__ != NULL && ptr != NULL)
    {
      Point->__del__(ptr);
      free(ptr);
      ptr = NULL;
    }
   return;
}
