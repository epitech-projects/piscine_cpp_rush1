/*
** vertex.c for  in /home/fritsc_h/piscine_cpp_rush1/ex_02
**
** Made by Fritsch harold
** Login   <fritsc_h@epitech.net>
**
** Started on  Sat Jan 11 23:34:12 2014 Fritsch harold
** Last update Sun Jan 12 05:00:38 2014 Jean Gravier
*/

#define _GNU_SOURCE
#include <stdio.h>
#include "object.h"
#include "vertex.h"
#include "raise.h"

typedef	struct
{
  Class base;
  int x, y, z;
}	VertexClass;

static	void	Vertex_ctor(Object *self, va_list *ap)
{
  if (self == NULL || ap == NULL)
    raise("Invalid pointer.");

  ((VertexClass *)self)->x = va_arg(ap, int);
  ((VertexClass *)self)->y = va_arg(ap, int);
  ((VertexClass *)self)->z = va_arg(ap, int);
}

static	void	Vertex_dtor(Object *self)
{
  if (self == NULL)
    raise("Invalid pointer.");
  ((Class*) self)->__init__ = NULL;
  ((Class*) self)->__del__ = NULL;
  ((Class*) self)->__str__ = NULL;
}

static	char const *Vertex_to_string_t(Object *self)
{
  char	*ret;

  if (self == NULL)
    raise("Invalid pointer.");

  asprintf(&ret, "<%s (%d, %d, %d)>", ((Class*)self)->__name__, ((VertexClass*)self)->x, ((VertexClass*)self)->y, ((VertexClass*)self)->z);
  return (ret);
}

static	VertexClass _description =
  {
    {sizeof(VertexClass), "Vertex", &Vertex_ctor, &Vertex_dtor, &Vertex_to_string_t},
    0, 0, 0};

Class	*Vertex = (Class*) &_description;
