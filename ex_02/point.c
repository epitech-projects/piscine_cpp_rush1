#define _GNU_SOURCE

#include <stdio.h>
#include "point.h"
#include "raise.h"

typedef struct
{
    Class base;
    int x, y;
} PointClass;

static void Point_ctor(Object* self, va_list *ap)
{
  if (self == NULL || ap == NULL)
    raise("Invalid pointer.");

  ((PointClass*)self)->x = va_arg(ap, int);
  ((PointClass*)self)->y = va_arg(ap, int);
}

static void Point_dtor(Object* self)
{
  if (self == NULL)
    raise("Invalid pointer.");
  ((Class*) self)->__init__ = NULL;
  ((Class*) self)->__del__ = NULL;
  ((Class*) self)->__str__ = NULL;
}

static char	const* Point_to_string_t(Object* self)
{
  char		*str;

  if (self == NULL)
    raise("Invalid pointer.");

  asprintf(&str, "<%s (%d, %d)>", ((Class*)self)->__name__, ((PointClass*)self)->x, ((PointClass*)self)->y);
  return (str);
}

static PointClass _description = {
  { sizeof(PointClass), "Point", &Point_ctor, &Point_dtor, &Point_to_string_t },
    0, 0
};

Class* Point = (Class*) &_description;
