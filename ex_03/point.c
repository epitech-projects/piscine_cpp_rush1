#define _GNU_SOURCE

#include <stdio.h>
#include "point.h"
#include "new.h"
#include "raise.h"

typedef struct
{
    Class base;
    int x, y;
} PointClass;

static void Point_ctor(Object* self, va_list *ap)
{
  if (self == NULL)
    raise("Invalid pointer.");
  ((PointClass*)self)->x = va_arg(ap, int);
  ((PointClass*)self)->y = va_arg(ap, int);
}

static void Point_dtor(Object* self)
{
  if (self == NULL)
    raise("Invalid pointer.");
  ((Class*) self)->__init__ = NULL;
  ((Class*) self)->__del__ = NULL;
  ((Class*) self)->__str__ = NULL;
}

static char	const* Point_to_string_t(Object* self)
{
  char		*str;

  if (self == NULL)
    raise("Invalid pointer.");
  asprintf(&str, "<%s (%d, %d)>", ((Class*)self)->__name__, ((PointClass*)self)->x, ((PointClass*)self)->y);
  return (str);
}

static	Object*	Point_add_operator_t(const Object* self, const Object* other)
{
  Object	*obj;
  int		x;
  int		y;

  if (self == NULL || other == NULL)
    raise("Invalid pointer.");
  x = ((PointClass*) self)->x + ((PointClass*) other)->x;
  y = ((PointClass*) self)->y + ((PointClass*) other)->y;
  obj = new(Point, x, y);
  return (obj);
}

static	Object*	Point_sub_operator_t(const Object* self, const Object* other)
{
  Object	*obj;
  int		x;
  int		y;

  if (self == NULL || other == NULL)
    raise("Invalid pointer.");
  x = ((PointClass*) self)->x - ((PointClass*) other)->x;
  y = ((PointClass*) self)->y - ((PointClass*) other)->y;
  obj = new(Point, x, y);
  return (obj);
}

static PointClass _description = {
  { sizeof(PointClass), "Point", &Point_ctor, &Point_dtor, &Point_to_string_t, &Point_add_operator_t, &Point_sub_operator_t },
    0, 0
};

Class* Point = (Class*) &_description;
